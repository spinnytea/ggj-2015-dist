Pop Tarts
=========

## The Sword or The Pop [Tart]

Created for Game Jam

http://globalgamejam.org/2015/games/pop-tarts


## Combat module
* monster
    * speed?
    * attack rate
    * damage
    * vulnerabilities
* weapon
    * speed
    * attack rate
    * damage
* what is the meaningful experience
Voting module

### Object structure

 Base -> Monster -> Chicken

 Base -> Weapon -> Sword

 Base -> Player
 
 Base -> Scene


## How to update your git project

### master -> jon

* git checkout master
* git pull
* git checkout jon
* git merge
* git push

### jon -> master

* git checkout master
* git rebase jon
* git push
* git checkout jon


## TODOs
* Sound
    * Player Hit
    * Player Death
* Images
    * Hit Animation/Image
    * Rabid Bunny Explosion
    * Curtains between stages
* Audience
    * When the audience votes, play that sound of the weapon
    * Audience needs to see the game
* Icicle should cancel Fireball (both stop attacking)
* Play-test: Game balance
    * ?Swap player controls?
    * Chickens are lame
    * killing rabbits gives you good luck
    * Poptart box
* Bugs
    * UI overflows into the game screen
