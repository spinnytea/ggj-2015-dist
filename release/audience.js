var angular = require('angular');
var io = require('socket.io');

module.exports = angular.module('pop_tart.audience', [])
  .controller('pop_tart.audience.AudienceController', AudienceController)
  .name;

AudienceController.$inject = ['$scope', '$location'];

function AudienceController ($scope, $location) {
  var audience = this;

  audience.config = [];

  var socket = io('http://' + $location.host() + ':3001');
  socket.on('startVote', function (config) {
    $scope.$apply(function () {
      audience.config = config;
    });
  });

  audience.vote = function (key) {
    socket.emit('vote', key);
  }
}