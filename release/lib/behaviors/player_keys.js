'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.player_keys', [])
  .service('pop_tart.behaviors.player_keys', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);


//
// A simple movement pattern based on key presses
// the arrows move you in the direction they face
//
PBehavior.prototype.update = function(obj, f5, dt) {
  if(obj.state !== 'move') return;

  if(f5.input.keys.left) {
    obj.x = Math.max(obj.x - obj.speed * dt/1e3, 0);
    obj.r = Math.PI;
  }
  if(f5.input.keys.right) {
    obj.x = Math.min(obj.x + obj.speed * dt/1e3, f5.width);
    obj.r = 0;
  }
  if(f5.input.keys.up) {
    obj.y = Math.max(obj.y - obj.speed * dt/1e3, 0);
    obj.r = -Math.PI/2;
  }
  if(f5.input.keys.down) {
    obj.y = Math.min(obj.y + obj.speed * dt/1e3, f5.height);
    obj.r = Math.PI/2;
  }
  if(f5.input.keys.shift){
    f5.music.stop();
  }
};
