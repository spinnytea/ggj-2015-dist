'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.attack.corneredRabbit', [])
  .service('pop_tart.behaviors.attack.corneredRabbit', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5) {
  var scene = f5.registry.scene;
  var dist = obj.radius*2;

	var nearLeft = (obj.x - scene.xLeft()) < dist;
  var nearRight = (scene.xRight() - obj.x) < dist;
  var nearTop = (obj.y - scene.yTop()) < dist;
  var nearBottom = (scene.yBottom() - obj.y) < dist;

  // Attack player if cornered
  if ((nearLeft || nearRight) && (nearTop || nearBottom))
  {
//    f5.resource.sound['monster.chickencat'].play();
    // Remove avoid behavior
    obj.$behavior = obj.$behavior.filter(function(b){return b.service !== 'pop_tart.behaviors.avoid';});
    obj.$behavior.push({ service: 'pop_tart.behaviors.follow' });

    obj.weapon.radius = 40;

    console.log('Cornered Rabbit attack!');
  }
  
  
};
