'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.wander', [])
  .service('pop_tart.behaviors.wander', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

//
// unintelligible random walk
// move in a random direction at a random speed every update cycle
// (for testing only)
//
PBehavior.prototype.update = function(obj, f5, dt) {
  var speed = 200;
  var dist = obj.radius * 1.1;

  var scene = f5.registry.scene;


  if ((obj.x - scene.xLeft() < dist) ||
      (scene.xRight() - obj.x < dist) ||
      (obj.y - scene.yTop() < dist) ||
      (scene.yBottom() - obj.y < dist)) {
         obj.r += Math.PI / 2;
  }

  obj.x += Math.cos(obj.r) * speed * dt/1e3;
  obj.y += Math.sin(obj.r) * speed * dt/1e3;
};
