'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.avoid', [])
  .service('pop_tart.behaviors.avoid', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5, dt) {
  var speed = 200;

  var player = f5.registry.player;
  //var r = Math.random() * Math.PI * 2;
	
	var distx = player.x - obj.x;
	var disty = player.y - obj.y;
  
  // Run away from player if too close
  if (Math.sqrt(distx * distx + disty * disty) < (player.radius + obj.radius))
  {
    if (obj.roar && !f5.resource.sound[obj.roar].playing()) {
      f5.resource.sound[obj.roar].play();
    }
	  obj.x += ((distx>0)?-1:1) * (speed * dt/1e3);
	  obj.y += ((disty>0)?-1:1) * (speed * dt/1e3);
  }
  
  
};
