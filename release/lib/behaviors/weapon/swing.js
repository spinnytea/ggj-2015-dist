'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.weapon.swing', [])
  .service('pop_tart.behaviors.weapon.swing', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

// States
//   move -> attack -> move
PBehavior.prototype.update = function(obj, f5, dt) {
  if(obj.state === 'move') {
    // move to the next state
    if(obj.shouldFire(f5)) {
      obj.state = 'attack';
      obj.myR = Math.PI/4;
      // play or restart sfx
      if (obj.attackSFX) {
        if (f5.resource.sound[obj.attackSFX].playing()) {
          f5.resource.sound[obj.attackSFX].stop();
          f5.resource.sound[obj.attackSFX].play();
        } else {
          f5.resource.sound[obj.attackSFX].play();
        }
      }
    }
  } else if(obj.state === 'attack') {
    // update the position

    if(obj.myR > -Math.PI/4) {
      // until we get to the end of the arc, keep swinging
      obj.myR -= obj.speed * dt/1e3;
      obj.myX = Math.cos(obj.r) * obj.extend;
      obj.myY = Math.sin(obj.r) * obj.extend;
    } else {
      obj.state = 'move';

      // come back to center
      obj.myR = 0;
      obj.myX = 0;
      obj.myY = 0;
    }
  }
};
