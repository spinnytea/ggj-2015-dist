'use strict';
var util = require('util');
var Stage = require('force5/core/stage');

var Scene = require('../objects/scene/scene');
var Player = require('../objects/player');
var Sword = require('../objects/weapon/sword');

var Dragon = require('../objects/monster/dragon');
var Chicken = require('../objects/monster/chicken');
var Bunny = require('../objects/monster/bunny');

module.exports = require('angular').module('pop_tart.stage.skip_vote', [])
  .service('pop_tart.stage.skip_vote', PStage).name;

function PStage() {
  Stage.call(this);
}
util.inherits(PStage, Stage);

//
// the lobby is in charge of building the arena
// however, the lobby uses player/voter selection
//
// this stage skips the vote and builds a default game
//
PStage.prototype.enter = function(f5) {

  f5.registry.monsters = [
    new Bunny(),
    new Dragon(),
    new Chicken()
  ];

  f5.registry.player = new Player({ x: 0, y: 0, r: Math.PI/4 });
  f5.registry.player.weapon = new Sword(f5.registry.player);

  f5.registry.scene = new Scene('scene.grass', 'scene.background_drums');

  f5.changeStage('pop_tart.stage.arena');
};
