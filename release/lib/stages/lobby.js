'use strict';
var util = require('util');
var _ = require('lodash');

var Stage = require('force5/core/stage');
var Scene = require('../objects/scene/scene');
var Player = require('../objects/player');

var Sword = require('../objects/weapon/sword');
var PopTart = require('../objects/weapon/poptart');
var Icicle = require('../objects/weapon/icicle');

var Bunny = require('../objects/monster/bunny');
var Chicken = require('../objects/monster/chicken');
var Dragon = require('../objects/monster/dragon');

module.exports = require('angular').module('pop_tart.stage.lobby', [])
  .service('pop_tart.stage.lobby', PStage).name;

function PStage() {
  Stage.call(this);
}
util.inherits(PStage, Stage);
PStage.$inject = ['pop_tart.behaviors.voting'];

PStage.prototype.enter = function(f5) {
  f5.registry.player = new Player(undefined);
  f5.registry.player.$behavior.push({ service: 'pop_tart.behaviors.weapon.checkCollision' });

  f5.audience.startVote([
    { title: 'Pick the Sword', value: 'sword' },
    { title: 'Pick the Pop Tart', value: 'poptart' },
    { title: 'Pick the Icicle', value: 'icicle' }
  ]);

  f5.registry.monsters = [
    new SelectEnemy(250, 'monster.bunny.head', this, f5, function() {
      var list = [];
      for(var i=0; i<10; i++)
        list.push(new Bunny());
      return list;
    }),
    new SelectEnemy(500, 'monster.chicken.head', this, f5, function() {
      return [ new Chicken(), new Chicken() ];
    }),
    new SelectEnemy(750, 'monster.dragon.head', this, f5, function() {
      return [ new Dragon() ];
    })
  ];

  f5.registry.scene = new Scene('scene.lobby');
  f5.music.play('scene.7thSaga_short');

  this.state = 'move';

  this.templateUrl = '/template/lobby.html';
};

PStage.prototype.leave = function(f5) {
  f5.audience.closeVote();
  //f5.music.stop();
};

PStage.prototype.update = function(f5, dt) {
  if(this.state === 'exiting') {
    // TODO slide curtain
    // TODO block monster/enemy movement

    f5.registry.scene.exit_pause -= dt;
    if(f5.registry.scene.exit_pause < 0) {
      // prepare the scene
      var scenes = ['scene.grass', 'scene.ice', 'scene.fire'];
      var scene = scenes[Math.floor(Math.random()*scenes.length)];
      f5.registry.scene = new Scene(scene, 'scene.background_drums');

      f5.registry.scene.buff(f5);

      // "let's take this outside"
      // go to the arena
      f5.changeStage('pop_tart.stage.arena');
    }
  }
};


function SelectEnemy(x, image, lobby, f5, enemyFn) {
  this.x = x;
  this.y = 200;
  this.radius = 80;
  this.r = 0;
  this.image = image;
  this.lobby = lobby;

  Object.defineProperty(this, 'f5', { get: function() { return f5; } });
  this.enemyFn = enemyFn;
}

SelectEnemy.prototype.processHit = function() {

  // remove a behavior from the player
  var idx = -1;
  this.f5.registry.player.$behavior.forEach(function(b, i) {
    if(b.service === 'pop_tart.behaviors.weapon.checkCollision')
      idx = i;
  });
  this.f5.registry.player.$behavior.splice(idx, 1);


  // pick a vote randomly
  var votes = this.f5.audience.votes;
  // first tally up the total votes
  idx = _.reduce(votes, function(s, value) {
    return s + value;
  }, 0);
  // pick a random number less than idx
  var winner = 'sword';
  idx = Math.floor(Math.random() * idx);
  // now pick a winner based on the random number
  // this is a weighted chance
  _.some(votes, function(value, key) {
    if(value > idx) {
      winner = key;
      return true;
    }
    idx -= value;
    return false;
  });
  // now give the player a weapon based on the winner
  if(winner === 'icicle') {
    this.f5.registry.player.weapon = new Icicle(this.f5.registry.player);
  } else if(winner === 'poptart') {
    this.f5.registry.player.weapon = new PopTart(this.f5.registry.player);
  } else {
    this.f5.registry.player.weapon = new Sword(this.f5.registry.player);
  }


  // load the enemies
  this.f5.registry.monsters = this.enemyFn();


  // start the transition to the next stage
  this.lobby.state = 'exiting';
};
