'use strict';
var _ = require('lodash');

module.exports = Base;

function Base(options) {
  _.merge(this, options);

  // move
  // - is when the player is in movement mode
  // attack
  // - an attack/animation is in progress
  // dying
  this.state = 'move';

  this.$behavior = [];
}
