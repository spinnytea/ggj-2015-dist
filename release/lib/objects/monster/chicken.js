'use strict';
var util = require('util');
var Monster = require('./monster');
var Egg = require('../weapon/egg');

module.exports = Chicken;

function Chicken() {
  Monster.call(this, {
    radius: 100,
    image: 'monster.chicken',
    roar: 'monster.chickencat',

    weapon: new Egg(this)
  });

  // pick a random direction to move
  this.r = Math.random() * Math.PI * 2;
  this.$behavior.push({ service: 'pop_tart.behaviors.wander' });

  this.$behavior.push({ service: 'pop_tart.behaviors.attack.layEgg' });
}
util.inherits(Chicken, Monster);

Chicken.prototype.processHit = function(weapon){
  this.health -= weapon.damage;
  console.log('Chicken hit by ' + weapon.image);
  console.log('Health: ' + this.health + ' ' + this.state);

  if (this.health <= 0){
    this.state = 'dying';
  }
};
