'use strict';
var util = require('util');
var Monster = require('./monster');

module.exports = MadChicken;

function MadChicken() {
  Monster.call(this, {
    radius: 100,
    image: 'monster.chicken'
  });

//    this.$behavior = this.$behavior.filter(function(behavior){return behavior.service !== 'poptart.behaviors.wander'});
    this.$behavior = [];
    this.$behavior.push({ service: 'pop_tart.behaviors.follow' });
}
util.inherits(MadChicken, Monster);
