'use strict';
var util = require('util');
var _ = require('lodash');
var Base = require('../base');
var Bunny = require('../monster/bunny');

module.exports = Scene;

function Scene(image, music) {
  Base.call(this, _.merge({
    x: 500,
    y: 500,
    radius: 500,
    r: 0,
    image: image,
    music: music,
    exit_pause: 0,
    state: 'entering'
  }));

  this.$behavior.push({ service: 'pop_tart.behaviors.stay_inside' });
}
util.inherits(Scene, Base);

Scene.prototype.yTop = function() {
    return this.y-this.radius;
};

Scene.prototype.yBottom = function() {
    return this.y+this.radius;
};

Scene.prototype.xLeft = function() {
    return this.x-this.radius;
};

Scene.prototype.xRight = function() {
    return this.x+this.radius;
};

Scene.prototype.buff = function(f5) {
  var player = f5.registry.player;

  switch(this.image) {
    case 'scene.grass':
      if(player.weapon.type === 'poptart') {
        player.weapon.distance *= 2;
        player.weapon.speed *= 1.5;
      }
      if(player.weapon.type === 'sword') {
        player.weapon.radius *= 1.5;
        player.weapon.extend *= 2;
      }
      for(var i=0; i<10; i++)
        f5.registry.monsters.push(new Bunny());
      break;
    case 'scene.ice':
      if(player.weapon.type === 'icicle')
        player.weapon.radius *= 2;

      f5.registry.monsters.forEach(function(m) {
        if(m.weapon.type === 'fireball')
          m.weapon.radius /= 1.5;
      });
      break;
    case 'scene.fire':
      if(player.weapon.type === 'icicle')
        player.weapon.radius /= 1.5;
      if(player.weapon.type === 'poptart')
        player.weapon.radius *= 2;

      f5.registry.monsters.forEach(function(m) {
        if(m.weapon.type === 'fireball')
          m.weapon.radius *= 2;
      });
      break;
  }
};