'use strict';
var util = require('util');
var _ = require('lodash');
var Base = require('../base');

module.exports = Weapon;

function Weapon(owner, options) {
  Base.call(this, _.merge({
    // defaults

    myX: 0,
    myY: 0,
    myR: 0,

    damage: 1

  }, options));

  Object.defineProperty(this, 'attacked', { enumerable: false, value: [] });

  // base the weapon position on the player
  // when updating the weapon, you *should* use myX and myY since they are local to the weapon
  // but if you do math against the x/y, then the setter will adjust
  Object.defineProperty(this, 'x', {
    get: function() { return owner.x + this.myX; },
    set: function(x) { this.myX = x - owner.x; }
  });
  Object.defineProperty(this, 'y', {
    get: function() { return owner.y + this.myY; },
    set: function(y) { this.myY = y - owner.y; }
  });
  Object.defineProperty(this, 'r', {
    get: function() { return owner.r + this.myR; },
    set: function(r) { this.myR = r - owner.r; }
  });

  // pass through the state from weapon to player
  Object.defineProperty(this, 'state', {
    get: function() { return owner.state; },
    set: function(s) {
      owner.state = s;
      this.attacked.splice(0);

      if(this.hasOwnProperty('passive_image') && this.hasOwnProperty('active_image')) {
        if(s === 'attack')
          this.image = this.active_image;
        if(s === 'move')
          this.image = this.passive_image;
      }
    }
  });
}
util.inherits(Weapon, Base);
