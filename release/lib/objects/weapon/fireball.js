'use strict';
var util = require('util');
var Weapon = require('./weapon');

module.exports = Fireball;

function Fireball(owner) {
  Weapon.call(this, owner, {
    type: 'fireball',
    radius: 40,
    r: 0,
    myR: -Math.PI/2,

    image: '',
    passive_image: '',
    active_image: 'weapon.fireball',

    damage: 20,

    shouldFire: function() { return Math.random() < 0.05; },
    distance: 900,
    speed: 1200
  });

  this.$behavior.push({ service: 'pop_tart.behaviors.jab' });
  this.$behavior.push({ service: 'pop_tart.behaviors.weapon.checkPlayerCollision' });
}
util.inherits(Fireball, Weapon);
