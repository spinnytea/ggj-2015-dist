'use strict';
var util = require('util');
var Weapon = require('./weapon');

module.exports = Sword;

function Sword(owner) {
  Weapon.call(this, owner, {
    type: 'sword',
    radius: 40,
    r: 0,
    image: 'weapon.sword',
    attackSFX: 'weapon.sword_attack',

    damage: 10,

    shouldFire: function(f5) { return f5.input.keys.space; },
    extend: 80,
    speed: Math.PI*4
  });

  this.$behavior.push({ service: 'pop_tart.behaviors.weapon.swing' });
  this.$behavior.push({ service: 'pop_tart.behaviors.weapon.checkCollision' });
}
util.inherits(Sword, Weapon);
