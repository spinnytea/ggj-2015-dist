'use strict';
require('angular').module('pop_tart', [
  require('force5'),
  require('./audience'),
  require('./lib/behaviors'),
  require('./lib/stages')
])
  .factory('$exceptionHandler', function() {
    return function(exception, cause) {
      if(cause) { exception.message += ' (caused by "' + cause + '")'; }
      throw exception;
    };
  });
