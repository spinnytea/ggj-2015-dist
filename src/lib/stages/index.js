'use strict';
module.exports = require('angular').module('pop_tart.stages', [
  require('./arena'),
  require('./lobby'),
  require('./skip_vote'),
  require('./entry')
]).name;