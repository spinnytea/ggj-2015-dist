'use strict';
var util = require('util');
var Stage = require('force5/core/stage');

module.exports = require('angular').module('pop_tart.stage.arena', [])
  .service('pop_tart.stage.arena', PStage).name;

function PStage() {
  Stage.call(this);
}
util.inherits(PStage, Stage);

PStage.prototype.enter = function(f5) {
  this.templateUrl = '/template/arena.html';

  f5.music.play(f5.registry.scene.music);
};

PStage.prototype.leave = function(f5) {
  f5.music.stop();
};

PStage.prototype.update = function(f5) {
  if(f5.registry.monsters.length === 0) {
    f5.resource.sound['scene.success_bad'].play();
    f5.changeStage('pop_tart.stage.lobby');
  }

  if(f5.registry.player.state === 'dying') {
    f5.resource.sound['scene.success_good'].play();
    f5.changeStage('pop_tart.stage.lobby');
  }
};