'use strict';
module.exports = require('angular').module('pop_tart.behaviors', [
  require('./voting'),
  require('./player_keys'),
  require('./player_keysII'),
  require('./wander'),
  require('./follow'),
  require('./avoid'),

  require('./stay_inside'),
  require('./face_player'),

  require('./quickDeath'),

  require('./weapon/swing'),
  require('./weapon/throw'),
  require('./weapon/jab'),

  require('./weapon/checkCollision'),
  require('./weapon/checkPlayerCollision'),

  require('./attack/corneredRabbit'),
  require('./attack/layEgg'),
  require('./attack/hatchEgg')
]).name;
