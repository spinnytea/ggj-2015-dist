'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.weapon.checkCollision', [])
  .service('pop_tart.behaviors.weapon.checkCollision', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5) {

  var monsters = f5.registry.monsters;

  monsters.forEach(function(monster){
    if(obj.hasOwnProperty('attacked') && obj.attacked.indexOf(monster) !== -1) return;

    var distx = obj.x - monster.x;
    var disty = obj.y - monster.y;
    if ((distx*distx + disty*disty) < (monster.radius * monster.radius)){
      monster.processHit(obj);

      if(obj.hasOwnProperty('attacked'))
        obj.attacked.push(monster);
    }
  });
};
