'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.weapon.throw', [])
  .service('pop_tart.behaviors.weapon.throw', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

// States
//   move -> attack -> move
PBehavior.prototype.update = function(obj, f5, dt) {
  if(obj.state === 'move') {
    // move to the next state
    if(obj.shouldFire(f5)) {
      obj.state = 'attack';
      // play or restart sfx
      if (obj.attackSFX) {
        if (f5.resource.sound[obj.attackSFX].playing()) {
          f5.resource.sound[obj.attackSFX].stop();
          f5.resource.sound[obj.attackSFX].play();
        } else {
          f5.resource.sound[obj.attackSFX].play();
        }
      }
    }
  } else if(obj.state === 'attack') {
    // update the position
    if(Math.sqrt(Math.pow(obj.myX, 2) + Math.pow(obj.myY, 2)) < obj.distance) {
      obj.myR += obj.turn_speed * dt/1e3;

      var r = obj.r - obj.myR;
      obj.myX += Math.cos(r) * obj.speed * dt/1e3;
      obj.myY += Math.sin(r) * obj.speed * dt/1e3;

    } else {
      obj.state = 'move';

      // come back to center
      obj.myR = 0;
      obj.myX = 0;
      obj.myY = 0;
    }
  }
};

