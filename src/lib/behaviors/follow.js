'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.follow', [])
  .service('pop_tart.behaviors.follow', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);

PBehavior.prototype.update = function(obj, f5, dt) {
  var speed = 200;

  var player = f5.registry.player;

	var distx = player.x - obj.x;
	var disty = player.y - obj.y;
  if ((distx * distx + disty * disty) > 16) {
    obj.x += ((distx>0)?1:-1) * (speed * dt/1e3);
    obj.y += ((disty>0)?1:-1) * (speed * dt/1e3);
  }
};
