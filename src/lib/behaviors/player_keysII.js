'use strict';
var util = require('util');
var Behavior = require('force5/core/behavior');

module.exports = require('angular').module('pop_tart.behaviors.player_keysII', [])
  .service('pop_tart.behaviors.player_keysII', PBehavior).name;

function PBehavior() {
  Behavior.call(this);
}
util.inherits(PBehavior, Behavior);


//
// A simple movement pattern based on key presses
// the arrows move you in the direction they face
//
PBehavior.prototype.update = function(obj, f5, dt) {
  if(obj.state !== 'move') return;

  if(f5.input.keys.left)
    obj.r -= obj.turn_speed * dt/1e3;
  if(f5.input.keys.right)
    obj.r += obj.turn_speed * dt/1e3;
  if(f5.input.keys.up) {
    obj.x += Math.cos(f5.registry.player.r) * obj.speed * dt / 1e3;
    obj.y += Math.sin(f5.registry.player.r) * obj.speed * dt / 1e3;
    //obj.y = Math.max(obj.y - obj.speed * dt/1e3, 0);
  }
    if(f5.input.keys.down){
      obj.x -= Math.cos(f5.registry.player.r) * obj.speed * dt / 1e3;
      obj.y -= Math.sin(f5.registry.player.r) * obj.speed * dt / 1e3;
    }

  if(f5.input.keys.shift){
      f5.music.stop();
  }
};
