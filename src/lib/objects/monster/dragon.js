'use strict';
var util = require('util');
var Monster = require('./monster');
var Fireball = require('../weapon/fireball');


module.exports = Dragon;

function Dragon() {
  Monster.call(this, {
    radius: 150,
    image: 'monster.dragon',
    roar: 'monster.dragonlonghern',

    turn_speed: Math.PI*6,

    health: 25,

    weapon: new Fireball(this)
  });

  this.$behavior.push({ service: 'pop_tart.behaviors.face_player' });
}
util.inherits(Dragon, Monster);

Dragon.prototype.processHit = function(weapon){
  this.health -= weapon.damage;
  console.log('Dragon hit by ' + weapon.image);
  console.log('Health: ' + this.health + ' ' + this.state);

  if (this.health <= 0) {
    this.state = 'dying';
  }
};