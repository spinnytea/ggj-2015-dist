'use strict';
var util = require('util');
var Weapon = require('./weapon');

module.exports = PopTart;

function PopTart(owner) {
  Weapon.call(this, owner, {
    type: 'poptart',
    radius: 20,
    r: 0,

    image: 'weapon.poptart',

    attackSFX: 'weapon.poptart_attack',

    damage: 1,

    shouldFire: function(f5) { return f5.input.keys.space; },
    distance: 300,
    speed: 1500,
    turn_speed: Math.PI*20
  });

  this.$behavior.push({ service: 'pop_tart.behaviors.weapon.throw' });
  this.$behavior.push({ service: 'pop_tart.behaviors.weapon.checkCollision' });
}
util.inherits(PopTart, Weapon);
