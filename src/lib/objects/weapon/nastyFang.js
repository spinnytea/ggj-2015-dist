'use strict';
var util = require('util');
var Weapon = require('./weapon');

module.exports = NastyFang;

function NastyFang(owner) {
  Weapon.call(this, owner, {
    type: 'nasty_fang',
    radius: 10,
    myX: 30,
    myY: 10,

    r: 0,
    image: 'weapon.nastyfang',
    attackSFX: 'weapon.explosion',

    recoil: function() {
      this.state = 'dying';
    },

    damage: 20

  });

  this.$behavior.push({ service: 'pop_tart.behaviors.weapon.checkPlayerCollision' });
}
util.inherits(NastyFang, Weapon);
